﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using System;

namespace Otus.Teaching.Linq.Atm
{
    public class OutputATMService
    {
        ATMManager _manager;

        public OutputATMService(ATMManager manager)
        {
            if (manager == null)
            {
                throw new ArgumentNullException(nameof(manager));
            }

            _manager = manager;
        }

        public User GetUser()
        {
            Console.WriteLine("Login:");
            string login = Console.ReadLine();
            Console.WriteLine("Password:");
            string password = Console.ReadLine();
            var user = _manager.GetUserLoginAndPassword(login, password);
            Console.WriteLine(user.ToString());
            Console.WriteLine(Environment.NewLine);
            return user;
        }

        public void GetAllAccounts(User user)
        {
            var accounts = _manager.GetAccountsOfUser(user);
            foreach (var account in accounts)
            {
                Console.WriteLine(account);
            }
            Console.WriteLine(Environment.NewLine);
        }

        public void GetAccountsHistories(User user)
        {
            var accountHistories = _manager.GetAccountsHistoryOfUser(user);
            foreach (var accountHistory in accountHistories)
            {
                Console.WriteLine(accountHistory.Key);
                if (accountHistory.Value.Count == 0)
                {
                    Console.WriteLine("Account has no history operations");
                    continue;
                }

                foreach (var operationHistory in accountHistory.Value)
                {
                    Console.WriteLine(operationHistory);
                }
                Console.WriteLine(Environment.NewLine);
            }
        }

        public void GetAllInputOperationsByUser()
        {
            var inputOperations = _manager.GetAllInputOperationsByUser();
            foreach (var userOperations in inputOperations)
            {
                Console.WriteLine(userOperations.Key);
                foreach (var operation in userOperations.Value)
                {
                    Console.WriteLine(operation);
                }
                Console.WriteLine(Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }

        public void GetAllUsersByCashMinimum()
        {
            Console.Write("N = ");
            decimal cashMinimum;
            while (!Decimal.TryParse(Console.ReadLine(), out cashMinimum))
            {
                Console.WriteLine("It is not valid decimal");
            }

            var usersThatPassCashMinimum = _manager.GetAllUsersByCashMinimum(cashMinimum);
            foreach (var userThatPassesCashMinimum in usersThatPassCashMinimum)
            {
                Console.WriteLine(userThatPassesCashMinimum);
            }
            Console.WriteLine(Environment.NewLine);
        }
    }
}
