﻿using System;
using System.Linq;
using Otus.Teaching.Linq.Atm;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            try
            {
                var atmManager = CreateATMManager();
                var outputATMService = new OutputATMService(atmManager);

                System.Console.WriteLine($"1. Вывод информации о заданном аккаунте по логину и паролю;");
                var user = outputATMService.GetUser();

                System.Console.WriteLine($"2. Вывод данных о всех счетах заданного пользователя;");
                outputATMService.GetAllAccounts(user);

                System.Console.WriteLine($"3. Вывод данных о всех счетах заданного пользователя, " +
                                         $"включая историю по каждому счёту");
                outputATMService.GetAccountsHistories(user);

                System.Console.WriteLine($"4. Вывод данных о всех операциях пополнения " +
                                         $"счёта с указанием владельца каждого счёта;");
                outputATMService.GetAllInputOperationsByUser();

                System.Console.WriteLine($"5. Вывод данных о всех пользователях у которых " +
                                         $"на счёте сумма больше N (N задаётся из вне и может быть любой);");
                outputATMService.GetAllUsersByCashMinimum();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"An error was occured: {ex.Message}");
            }

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}