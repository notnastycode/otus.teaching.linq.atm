﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        private Account GetAccountById(int id)
        {
            var account = Accounts.SingleOrDefault(x => x.Id == id);
            if (account == null)
            {
                throw new InvalidOperationException("There is no such account");
            }
            return account;
        }

        private List<Account> GetAccountsByMinimumCash(decimal minimumMoney)
        {
            return Accounts.Where(x => x.CashAll > minimumMoney).ToList();
        }

        private User GetUserById(int id)
        {
            var user = Users.SingleOrDefault(x => x.Id == id);
            if (user == null)
            {
                throw new InvalidOperationException("There is no such user");
            }
            return user;
        }

        private User GetUserByAccountId(int accountId)
        {
            return GetUserById(GetAccountById(accountId).UserId);
        }

        public User GetUserLoginAndPassword(string login, string password)
        {
            var user = Users.SingleOrDefault(x => x.Login.Equals(login) && x.Password.Equals(password));
            if (user == null)
            {
                throw new InvalidOperationException("There is no such user");
            }
            return user;
        }

        public List<Account> GetAccountsOfUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }

            return Accounts.Where(x => x.UserId == user.Id).ToList();
        }

        public Dictionary<Account, List<OperationsHistory>> GetAccountsHistoryOfUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var accounts = GetAccountsOfUser(user);
            return accounts.GroupJoin(History, a => a.Id, h => h.AccountId, 
                (account , history) => KeyValuePair.Create(account, history))
                .ToDictionary(k => k.Key, v => v.Value.ToList());
        }

        public Dictionary<User, List<OperationsHistory>> GetAllInputOperationsByUser()
        {
            return History.Where(x => x.OperationType == OperationType.InputCash)
                          .GroupBy(x => GetUserByAccountId(x.AccountId))
                          .ToDictionary(k => k.Key, v => v.ToList());
        }

        public List<User> GetAllUsersByCashMinimum(decimal minimumOfMoney)
        {
            var accounts = GetAccountsByMinimumCash(minimumOfMoney);
            return accounts.Select(x => GetUserByAccountId(x.Id)).Distinct().ToList();
        }
    }
}